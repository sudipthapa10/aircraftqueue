package com.aircraft.acservice.controller;
import java.util.TreeSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.aircraft.acservice.exceptions.AircraftSizeMismatchException;
import com.aircraft.acservice.exceptions.AircraftTypeMismatchException;
import com.aircraft.acservice.model.Aircraft;
import com.aircraft.acservice.processor.DequeueRequest;
import com.aircraft.acservice.processor.EnqueRequest;
import com.aircraft.acservice.processor.QueueManager;
import com.aircraft.acservice.utils.Priority;

@RestController
public class AircraftController {
	
	@Autowired
	private QueueManager processor;
	
	
	@RequestMapping(value = "/service/crafts", method = RequestMethod.GET)
	public TreeSet<Aircraft> getCrafts() {
		return processor.getCrafts();
	}
	

	@RequestMapping(value = "/service/crafts", method = RequestMethod.POST)
	@ExceptionHandler({AircraftTypeMismatchException.class,AircraftSizeMismatchException.class}) 
	public void addAircraft(@RequestBody Aircraft craft) {
		if (!(craft.getType().equalsIgnoreCase("cargo") || craft.getType().equalsIgnoreCase("passenger"))) {
			throw new AircraftTypeMismatchException();
		}
		if (!(craft.getSize().equalsIgnoreCase("large") || craft.getSize().equalsIgnoreCase("small"))) {
			throw new AircraftSizeMismatchException();
		}
		
		int priority = Priority.caluclatePriority(craft);
		//set priority
		craft.setPriority(priority);
		processor.aqmRequestProcess(new EnqueRequest(craft));
			
	}
	
	@RequestMapping(value = "/service/crafts", method = RequestMethod.DELETE)
	public void remove() {
			processor.aqmRequestProcess(new DequeueRequest() );		
		}		

}
