**Build**

1.This is maven project using spring boot.
In order to run it, project need to be imported project as mvn project.

3.Build  project using maven build.Specify  goal as spring-boot:run




** Rest API**
1. System boot will start as soon as the project is built.
2. Rest Client can be used to communicate with server.

**GET**

GET request with http://localhost:8080/service/crafts will display list of results sorted by priority.

since it is not using persistence storage , At first you will see an empty array as response.


**POST**

 POST request with http://localhost:8080/service/crafts will push aircraft into the queue.Please set content type to application/json to post data
   post body will accept data in follwing format:
   
  { "id" : "pas123266",
  "type" : "passenger",
  "size" : "large"
}

  {
  "id" : "car234",
  "type" : "cargo",
  "size" : "large"
}

 "id" , "type" and "size" are all required fields.
 "type" should be either passenger or cargo otherwise it will throw an exception.
 "size" should be either small or large otherwise application will throw exception.
 once we pass these fields , application will internally calculate priority and timestamp.

**DELETE**

5. DELETE request with http://localhost:8080/service/craft will remove first element from the queue.

